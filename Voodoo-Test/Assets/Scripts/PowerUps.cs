using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour
{
    public GridManager GridManager;

    public Transform HammerButton;
    public Transform RocketButton;
    public Transform RocketSideButton;
    public Transform DiscoButton;

    private void OnEnable()
    {
        GridManager.OnPowerUpsUsed += StopAnimation;
    }

    public void HammerPower()
    {
        GridManager.HammerPower = true;
        HammerButton.transform.DOScale(transform.localScale * 1.1f, 0.2f).SetLoops(-1, LoopType.Yoyo);
    }

    public void RocketPower()
    {
        GridManager.RocketPower= true;
        RocketButton.transform.DOScale(transform.localScale * 1.1f, 0.2f).SetLoops(-1, LoopType.Yoyo);
    }

    public void RocketSidePower()
    {
        GridManager.RocketSidePower= true;
        RocketSideButton.transform.DOScale(transform.localScale * 1.1f, 0.2f).SetLoops(-1, LoopType.Yoyo);
    }

    public void DiscoPower()
    {
        GridManager.DiscoPower= true;
        DiscoButton.transform.DOScale(transform.localScale * 1.1f, 0.2f).SetLoops(-1, LoopType.Yoyo);
    }

    public void ShufflePower()
    {
        GridManager.UseShufflePower();
    }

    private void StopAnimation()
    {
        HammerButton.transform.DOKill();
        HammerButton.transform.localScale = Vector3.one;
        RocketButton.transform.DOKill();
        RocketButton.transform.localScale = Vector3.one;
        RocketSideButton.transform.DOKill();
        RocketSideButton.transform.localScale = Vector3.one;
        DiscoButton.transform.DOKill();
        DiscoButton.transform.localScale = Vector3.one;
    }

    private void OnDisable()
    {
        GridManager.OnPowerUpsUsed -= StopAnimation;
    }
}
