using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constantes
{
    public class UIScreenNames
    {
        public const string START_SCREEN = "StartScreen";
        public const string CHOOSE_GAME_SCREEN = "ChooseGameScreen";
        public const string IN_GAME_SCREEN = "InGameScreen";
        public const string END_SCREEN = "EndScreen";
    }

    public class Sounds
    {
        public const string BUTTON_CLICK = "BUTTON_CLICK";
        public const string MATCH_SOUND = "MATCH_SOUND";
        public const string WIN_SOUND = "WIN_SOUND";
        public const string LOSE_SOUND = "LOSE_SOUND";
    }

    public class Grid
    {
        public const int BALL_NUMBER = 36;
        public const int BALL_ROW_NUMBER = 6;
        public const int BALL_COLUMN_NUMBER = 6;
        public const int MINIMUM_NUMBER_OF_BALLS = 3;  //The minimum number of balls you need to make a match
    }

    public class PlayerPrefs
    {
        public const string EASY_LEVEL = "EasyLevel";
        public const string MEDIUM_LEVEL = "MediumLevel";
        public const string HARD_LEVEL = "HardLevel";
    }
}
