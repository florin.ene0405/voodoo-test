using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GridManager GridManager;

    public LevelsSOList EasyLevelsSO;
    public LevelsSOList MediumLevelsSO;
    public LevelsSOList HardLevelsSO;

    public GameObject LosePanel;

    private LevelSO _currentLevel;
    private string _currentLevelType;
    private int _levelIndex;


    public void StartEasyGame()
    {
        _currentLevelType = Constantes.PlayerPrefs.EASY_LEVEL;
        StartTheGame(EasyLevelsSO);
    }
    

    public void StartMediumGame()
    {
        _currentLevelType = Constantes.PlayerPrefs.MEDIUM_LEVEL;
        StartTheGame(MediumLevelsSO);
    }

    public void StartHardGame()
    {
        _currentLevelType = Constantes.PlayerPrefs.HARD_LEVEL;
        StartTheGame(HardLevelsSO);
    }

    private void StartTheGame(LevelsSOList levelList)
    {
        _levelIndex = PlayerPrefs.GetInt(_currentLevelType, 0);

        if (_levelIndex >= levelList.Levels.Count)
        {
            _levelIndex = 0;
            PlayerPrefs.SetInt(_currentLevelType, _levelIndex);
        }

        _currentLevel = levelList.Levels[_levelIndex];

        GridManager.StartTheGame(_currentLevel);
    }

    public void StartCustomLevel(LevelSO level)
    {
        GridManager.StartTheGame(level);
        UIManager.Instance.LoadScreen(Constantes.UIScreenNames.IN_GAME_SCREEN);
    }

    public LevelSO GetCurrentLevel() => _currentLevel;

    public void OnGameWin()
    {
        var index = PlayerPrefs.GetInt(_currentLevelType, 0);

        index++;

        PlayerPrefs.SetInt(_currentLevelType, index);

        UIManager.Instance.LoadScreen(Constantes.UIScreenNames.END_SCREEN);
        SoundManager.Instance.PlaySound(Constantes.Sounds.WIN_SOUND);
    }

    public void OnGameLose()
    {
        LosePanel.SetActive(true);
        SoundManager.Instance.PlaySound(Constantes.Sounds.LOSE_SOUND);
    }
}
