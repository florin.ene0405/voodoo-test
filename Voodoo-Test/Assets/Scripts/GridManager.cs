using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class GridManager : MonoBehaviour
{
    public GameManager GameManager;
    public LevelInfoController LevelInfoController;
    public GridSlotController GridSlotControllerPrefab;
    public BallController BallControllerPrefab;
    public List<ElementColor> ElementColors = new List<ElementColor>();
    public Slider Slider1;
    public Slider Slider2;
    public Image SliderImage1;
    public Image SliderImage2;
    public List<Color> SliderColors = new List<Color>();

    [HideInInspector] public bool HammerPower = false;
    [HideInInspector] public bool RocketPower = false;
    [HideInInspector] public bool RocketSidePower = false;
    [HideInInspector] public bool DiscoPower = false;

    public Action OnPowerUpsUsed;


    private List<GridSlotController> _allSlots = new List<GridSlotController>();
    private List<BallController> _allBalls = new List<BallController>();
    private List<BallController> _ballLinked = new List<BallController>();
    private BallController _lastBall;
    private bool _startLink = false;
    private string _linkedColor;
    private int _numberOfBallLinked = 0;
    private LevelSO _currentLevel;


    public void StartTheGame(LevelSO level)
    {
        for (int i = 0; i < Constantes.Grid.BALL_NUMBER; i++)
        {
            var element = Instantiate<GridSlotController>(GridSlotControllerPrefab, transform);
            element.Initialize(i / Constantes.Grid.BALL_ROW_NUMBER + 1, i % Constantes.Grid.BALL_COLUMN_NUMBER + 1);
            _allSlots.Add(element);
        }

        _currentLevel = level;

        LevelInfoController.Initialize(_currentLevel);

        PopulateTheSlots();
    }

    private void PopulateTheSlots()
    {
        for (int i = 0; i < _allSlots.Count; i++)
        {
            var newBall = Instantiate<BallController>(BallControllerPrefab, _allSlots[i].transform);
            var newColor = ElementColors[UnityEngine.Random.Range(0, ElementColors.Count)];
            newBall.Initialize(newColor);
            newBall.AllocateGridManage(this);
            _allBalls.Add(newBall);

            if (_currentLevel.SlotsBlocked.Contains(newBall.MyGridSlotController.Position))
            {
                newBall.MakeItCaged();
            }
        }
    }

    public void StartLink(BallController ballController)
    {
        if (HammerPower)
        {
            UseHammerPower(ballController);
            return;
        }

        if (RocketPower)
        {
            UseRocketPower(ballController);
            return;
        }

        if (RocketSidePower)
        {
            UseRocketSidePower(ballController);
            return;
        }

        if (DiscoPower)
        {
            UseDIscoPower(ballController);
            return;
        }

        if (ballController.IsCaged)
        {
            return;
        }

        _startLink = true;
        _ballLinked.Add(ballController);
        _linkedColor = ballController.ColorName;
        _lastBall = ballController;
        _numberOfBallLinked++;
        IncrementTheSlider();
    }

    public void UseHammerPower(BallController ballController)
    {
        _ballLinked.Add(ballController);

        StopLink(true);

        HammerPower = false;
        OnPowerUpsUsed?.Invoke();
    }

    public void UseDIscoPower(BallController ballController)
    {
        for (int i = 0; i < _allBalls.Count; i++)
        {
            if (string.Equals(_allBalls[i].ColorName, ballController.ColorName))
            {
                _ballLinked.Add(_allBalls[i]);
            }
        }

        StopLink(true);

        DiscoPower = false;
        OnPowerUpsUsed?.Invoke();
    }

    public void UseRocketPower(BallController ballController)
    {
        for (int i = 0; i < _allBalls.Count; i++)
        {
            if (_allBalls[i].MyGridSlotController.Position.x == ballController.MyGridSlotController.Position.x)
            {
                _ballLinked.Add(_allBalls[i]);
            }
        }

        StopLink(true);

        RocketPower = false;
        OnPowerUpsUsed?.Invoke();
    }

    public void UseRocketSidePower(BallController ballController)
    {
        for (int i = 0; i < _allBalls.Count; i++)
        {
            if (_allBalls[i].MyGridSlotController.Position.y == ballController.MyGridSlotController.Position.y)
            {
                _ballLinked.Add(_allBalls[i]);
            }
        }

        StopLink(true);

        RocketSidePower = false;
        OnPowerUpsUsed?.Invoke();
    }

    public void UseShufflePower()
    {
        List<ElementColor> allColors = new List<ElementColor>();

        for (int i = 0; i < _allBalls.Count; i++)
        {
            if (!_allBalls[i].IsCaged)
            {
                allColors.Add(_allBalls[i].MyElementColor);
            }
        }

        allColors = allColors.OrderBy(x => UnityEngine.Random.value).ToList();

        int ballsSkipped = 0;

        for (int i = 0; i < _allBalls.Count; i++)
        {
            if (_allBalls[i].IsCaged)
            {
                ballsSkipped++;
                continue;
            }

            _allBalls[i].Initialize(allColors[i - ballsSkipped]);
        }
    }

    public void StopLink(bool powerUps = false)
    {

        if (_numberOfBallLinked >= Constantes.Grid.MINIMUM_NUMBER_OF_BALLS || powerUps)
        {
            _ballLinked.ForEach(x => x.WasMatched());

            _ballLinked.ForEach(x => LevelInfoController.NewMatch(x));
            var orderList = _ballLinked.OrderBy(x => -x.MyGridSlotController.Position.y).ToList();
            orderList.ForEach(x => GetANewBall(x));

            LevelInfoController.NewMove(powerUps);

            if (!powerUps)
            {
                _ballLinked.ForEach(x => CheckForCagedBallsNear(x));
            }

            SoundManager.Instance.PlaySound(Constantes.Sounds.MATCH_SOUND);
        }


        _startLink = false;
        _ballLinked.ForEach(x => x.ResetLine());
        _ballLinked.Clear();
        _linkedColor = string.Empty;
        _numberOfBallLinked = 0;
        ResetTheSlider();
    }

    private void GetANewBall(BallController ballController)
    {
        if (ballController.IsCaged)
        {
            ballController.MakitUnCaged();
            return;
        }

        BallController newBall = GetTheBallAbove(ballController);

        if (newBall == null)
        {
            var newColor = ElementColors[UnityEngine.Random.Range(0, ElementColors.Count)];
            ballController.Initialize(newColor);
        }
        else
        {
            ballController.Initialize(newBall.MyElementColor);
            GetANewBall(newBall);
        }
    }

    private void CheckForCagedBallsNear(BallController ballController)
    {
        Vector2 ballPosition = ballController.MyGridSlotController.Position;
        List<BallController> neighborBalls = new List<BallController>();

        neighborBalls.Add(GetBallByPosition(new Vector2(ballPosition.x, ballPosition.y + 1)));
        neighborBalls.Add(GetBallByPosition(new Vector2(ballPosition.x, ballPosition.y - 1)));
        neighborBalls.Add(GetBallByPosition(new Vector2(ballPosition.x - 1, ballPosition.y)));
        neighborBalls.Add(GetBallByPosition(new Vector2(ballPosition.x + 1, ballPosition.y)));

        for (int i = 0; i < neighborBalls.Count; i++)
        {
            if (neighborBalls[i] != null && neighborBalls[i].IsCaged)
            {
                neighborBalls[i].MakitUnCaged();
            }
        }
    }

    private BallController GetBallByPosition(Vector2 position)
    {
        for (int i = 0; i < _allBalls.Count; i++)
        {
            if (_allBalls[i].MyGridSlotController.Position == position)
            {
                return _allBalls[i];
            }
        }

        return null;
    }

    private BallController GetTheBallAbove(BallController ballController)
    {
        if (ballController.MyGridSlotController.Position.y < Constantes.Grid.BALL_COLUMN_NUMBER)
        {
            for (int i = 0; i < _allBalls.Count; i++)
            {
                if (ballController.MyGridSlotController.Position.y + 1 == _allBalls[i].MyGridSlotController.Position.y && ballController.MyGridSlotController.Position.x == _allBalls[i].MyGridSlotController.Position.x)
                {
                    if (_allBalls[i].IsCaged)
                    {
                        return null;
                    }

                    if (_allBalls[i].CheckIfBallWasMatched())
                    {
                        GetANewBall(_allBalls[i]);
                    }

                    return _allBalls[i];
                }
            }
        }

        return null;
    }

    public void CheckForLink(BallController ballController)
    {
        if (!_startLink)
        {
            return;
        }

        if (_ballLinked.Contains(ballController))
        {
            return;
        }

        if (string.Equals(_linkedColor, ballController.ColorName))
        {
            if (CheckIfCanLink(_lastBall, ballController))
            {
                _ballLinked.Add(ballController);

                var oldBall = _lastBall.GetRectParentTransform().position;
                var newBall = ballController.GetRectParentTransform().position;

                Vector2 newPos = new Vector2((newBall.x - oldBall.x), (newBall.y - oldBall.y));
                _lastBall.UILineRenderer.points[1] = newPos;
                _lastBall.UILineRenderer.SetAllDirty();
                _lastBall.StopTracking();
                ballController.StartTracking();
                _lastBall = ballController;
                _numberOfBallLinked++;
                IncrementTheSlider();

                SoundManager.Instance.PlayMatchSound(_numberOfBallLinked - Constantes.Grid.MINIMUM_NUMBER_OF_BALLS);
            }
        }
    }

    private void IncrementTheSlider()
    {
        if (_numberOfBallLinked > Slider1.maxValue || _numberOfBallLinked < Constantes.Grid.MINIMUM_NUMBER_OF_BALLS)
        {
            return;
        }

        Slider1.value = _numberOfBallLinked;
        Slider2.value = _numberOfBallLinked;
        SliderImage1.color = SliderColors[_numberOfBallLinked - Constantes.Grid.MINIMUM_NUMBER_OF_BALLS];
        SliderImage2.color = SliderColors[_numberOfBallLinked - Constantes.Grid.MINIMUM_NUMBER_OF_BALLS];
    }

    private void ResetTheSlider()
    {
        Slider1.value = 0;
        Slider2.value = 0;
        SliderImage1.color = SliderColors[0];
        SliderImage2.color = SliderColors[0];
    }

    private bool CheckIfCanLink(BallController lastBall, BallController newBall)
    {
        if ((lastBall.MyGridSlotController.Position.x == newBall.MyGridSlotController.Position.x || lastBall.MyGridSlotController.Position.x == newBall.MyGridSlotController.Position.x + 1 || lastBall.MyGridSlotController.Position.x == newBall.MyGridSlotController.Position.x - 1) && (lastBall.MyGridSlotController.Position.y == newBall.MyGridSlotController.Position.y || lastBall.MyGridSlotController.Position.y == newBall.MyGridSlotController.Position.y + 1 || lastBall.MyGridSlotController.Position.y == newBall.MyGridSlotController.Position.y - 1))
        {
            return true;
        }

        return false;
    }
}

[System.Serializable]
public class ElementColor
{
    public string ColorName;
    public Color Color;
}
