using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelsSO", menuName = "ScriptableObjects/LevelsSO", order = 1)]
public class LevelsSOList : ScriptableObject
{
    public List<LevelSO> Levels = new List<LevelSO>(); 
}
