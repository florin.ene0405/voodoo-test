using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelSO", menuName = "ScriptableObjects/LevelSO", order = 0)]
public class LevelSO : ScriptableObject
{
    [Tooltip("Number of moves")]
    public int Moves;

    [Tooltip("What balls you have to destroy and how many")]
    public List<ColorBalls> ColorBalls = new List<ColorBalls>();

    [Tooltip("The slots that will be blocked")]
    public List<Vector2> SlotsBlocked = new List<Vector2>();
}

[System.Serializable]
public class ColorBalls
{
    public string ColorName;
    public Color Color;
    public int Number;
}
