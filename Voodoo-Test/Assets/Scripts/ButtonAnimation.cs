using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonAnimation : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public ButtonAnimationType ButtonAnimationType;

    private Vector3 _initialPosition;
    private Vector3 _initialRotation;
    private Vector3 _initialSize;


    protected void Start()
    {
        _initialPosition = transform.position;
        _initialRotation = transform.rotation.eulerAngles;
        _initialSize = transform.localScale;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        StartAnimation();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        StopAnimation();
    }

    private void StartAnimation()
    {
        switch (ButtonAnimationType)
        {
            case ButtonAnimationType.None:
                break;
            case ButtonAnimationType.Scale:
                DoScaleAnimation();
                break;
            case ButtonAnimationType.Shake:
                DoShakeAnimation();
                break;
            case ButtonAnimationType.ScaleAndShake:
                DoShakeAnimation();
                DoScaleAnimation();
                break;
            default:
                break;
        }
    }

    private void DoScaleAnimation()
    {
        transform.DOScale(transform.localScale * 1.2f, 0.2f);
    }

    private void DoShakeAnimation()
    {
        transform.DOShakeRotation(0.5f, Vector3.forward * 5, 30);
    }

    private void StopAnimation()
    {
        transform.DOKill();

        transform.position = _initialPosition;
        transform.rotation = Quaternion.Euler(_initialRotation);
        transform.localScale = _initialSize;
    }

    private void OnDisable()
    {
        StopAnimation();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        SoundManager.Instance.PlaySound(Constantes.Sounds.BUTTON_CLICK);
    }
}

public enum ButtonAnimationType
{
    None = 0,
    Scale = 1,
    Shake = 2,
    ScaleAndShake = 3
}