using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CustomPanelController : MonoBehaviour
{
    public GameManager GameManager;

    public TMP_InputField NumberOfMoves;
    public TMP_InputField Red;
    public TMP_InputField Blue;
    public TMP_InputField Green;
    public TMP_InputField Yellow;
    public TMP_InputField Purple;
    public TMP_InputField Cyan;
    public TMP_InputField NumberOfLocks;

    public void StartCustomLevel()
    {
        LevelSO newLevel = new LevelSO();

        int numberOfMoves = 10;
        int numberOfLocks = 0;
        int red = 0;
        int blue = 0;
        int green = 0;
        int yellow = 0;
        int magenta = 0;
        int cyan = 0;

        if (int.TryParse(Red.text, out red))
        {
            newLevel.ColorBalls.Add(new ColorBalls { ColorName = "Red", Color = Color.red, Number = red });
        }
        
        if (int.TryParse(Blue.text, out blue))
        {
            newLevel.ColorBalls.Add(new ColorBalls { ColorName = "Blue", Color = Color.blue, Number = blue });
        }

        if (int.TryParse(Green.text, out green))
        {
            newLevel.ColorBalls.Add(new ColorBalls { ColorName = "Green", Color = Color.green, Number = green });
        }

        if (int.TryParse(Yellow.text, out yellow))
        {
            newLevel.ColorBalls.Add(new ColorBalls { ColorName = "Yellow", Color = Color.yellow, Number = yellow });
        }

        if (int.TryParse(Purple.text, out magenta))
        {
            newLevel.ColorBalls.Add(new ColorBalls { ColorName = "Purple", Color = Color.magenta, Number = magenta });
        }

        if (int.TryParse(Cyan.text, out cyan))
        {
            newLevel.ColorBalls.Add(new ColorBalls { ColorName = "Cyan", Color = Color.cyan, Number = cyan });
        }

        if (int.TryParse(NumberOfMoves.text, out numberOfMoves))
        {

            newLevel.Moves = numberOfMoves;
        }

        if (int.TryParse(NumberOfLocks.text, out numberOfLocks))
        {
            for (int i = 0; i < numberOfLocks; i++)
            {
                newLevel.SlotsBlocked.Add(new Vector2(Random.Range(0, Constantes.Grid.BALL_ROW_NUMBER), Random.Range(0, Constantes.Grid.BALL_COLUMN_NUMBER)));
            }
        }

        GameManager.StartCustomLevel(newLevel);
    }
}
