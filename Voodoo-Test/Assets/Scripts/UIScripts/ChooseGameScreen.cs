using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ChooseGameScreen : UIScreen
{
    public GameManager GameManager;

    public TextMeshProUGUI EasyText;
    public TextMeshProUGUI MediumText;
    public TextMeshProUGUI HardText;

    private void OnEnable()
    {
        EasyText.text = PlayerPrefs.GetInt(Constantes.PlayerPrefs.EASY_LEVEL, 0).ToString() + "/" + GameManager.EasyLevelsSO.Levels.Count.ToString();
        MediumText.text = PlayerPrefs.GetInt(Constantes.PlayerPrefs.MEDIUM_LEVEL, 0).ToString() + "/" + GameManager.MediumLevelsSO.Levels.Count.ToString();
        HardText.text = PlayerPrefs.GetInt(Constantes.PlayerPrefs.HARD_LEVEL, 0).ToString() + "/" + GameManager.HardLevelsSO.Levels.Count.ToString();
    }

    public void StartGameEasy()
    {
        GameManager.StartEasyGame();
        LoadInGameScreen();
    }

    public void StartGameMedium()
    {
        GameManager.StartMediumGame();
        LoadInGameScreen();
    }
    
    public void StartGameHard()
    {
        GameManager.StartHardGame();
        LoadInGameScreen();
    }

    private void LoadInGameScreen()
    {
        UIManager.Instance.LoadScreen(Constantes.UIScreenNames.IN_GAME_SCREEN);
    }
}
