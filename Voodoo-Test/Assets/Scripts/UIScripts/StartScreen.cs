using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartScreen : UIScreen
{
    public GameObject ExitPanel;


    public void ShowExitPanel()
    {
        if (ExitPanel == null)
        {
            Debug.LogError("ExitPanel is null in " + this);
            return;
        }

        ExitPanel.SetActive(true);
    }

    public void HideExitPanel()
    {
        if (ExitPanel == null)
        {
            Debug.LogError("ExitPanel is null in " + this);
            return;
        }

        ExitPanel.SetActive(false);
    }

    public void RestartTheGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ChooseGameScreen()
    {
        UIManager.Instance.LoadScreen(Constantes.UIScreenNames.CHOOSE_GAME_SCREEN);
    }

    public void ExitTheGame()
    {
        Application.Quit();
    }
}
