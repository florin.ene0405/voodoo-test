using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSlotController : MonoBehaviour
{
    public Vector2 Position;

    public void Initialize(int x, int y)
    {
        Position.x = x;
        Position.y = y;

        transform.name = $"{x} - {y}";
    }
}
