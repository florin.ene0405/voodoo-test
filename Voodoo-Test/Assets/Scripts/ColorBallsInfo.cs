using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ColorBallsInfo : MonoBehaviour
{
    public Image MyImage;
    public TextMeshProUGUI TextCounter;
    public string ColorName;
    public int Counter;

    public void Initialize(Color color, int number,string colorName)
    {
        MyImage.color = color;
        Counter = number;
        ColorName = colorName;
        TextCounter.text = "x" + Counter.ToString();
    }

    public void LowerTheMatchedNeeded()
    {
        Counter --;

        if (Counter < 0)
        {
            Counter = 0;
        }

        TextCounter.text = "x" + Counter.ToString();
    }
}
