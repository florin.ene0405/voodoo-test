using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class BallController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler
{
    public UILineRenderer UILineRenderer;
    public Image MyImage;
    public string ColorName;
    public GridSlotController MyGridSlotController;
    public ElementColor MyElementColor;
    public GameObject Cage;
    public bool IsCaged { get => _isCaged; }

    private bool _isCaged = false;
    private bool _isPointerDown = false;
    private bool _wasMatched = false;
    private RectTransform _rectTransform;
    private RectTransform _parentRectTransform;
    private GridManager _gridManager;
    private Vector3 _initialSize;

    private void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        _parentRectTransform = transform.parent.GetComponent<RectTransform>();
        _initialSize = transform.localScale;
    }

    public void Initialize(ElementColor element)
    {
        MyElementColor = element;

        MyImage.color = element.Color;
        ColorName = element.ColorName;
        UILineRenderer.color = element.Color;
        _wasMatched = false;

        MyGridSlotController = transform.parent.GetComponent<GridSlotController>();
    }

    public void MakeItCaged()
    {
        _isCaged = true;
        Cage.SetActive(true);
    }

    public void MakitUnCaged()
    {
        _isCaged = false;
        Cage.SetActive(false);
    }

    public void AllocateGridManage(GridManager gridManager)
    {
        _gridManager = gridManager;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!_isCaged)
        {
            StartTracking();
        }

        _gridManager.StartLink(this);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        StopTracking();
        _gridManager.StopLink();
    }

    private void Update()
    {
        if (_isPointerDown)
        {
            Vector2 screenPoint = Input.mousePosition;
            Vector2 localPoint;

            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform, screenPoint, GetComponent<Camera>(), out localPoint))
            {
                UILineRenderer.points[1] = localPoint;
                UILineRenderer.SetAllDirty();
            }
        }
    }

    public void StopTracking()
    {
        _isPointerDown = false;
    }

    public void StartTracking()
    {
        _isPointerDown = true;
        StartAnimation();
    }

    private void StartAnimation()
    {
        StopAnimation();
        transform.DOScale(transform.localScale * 1.1f, 0.2f).SetLoops(-1, LoopType.Yoyo);
    }

    private void StopAnimation()
    {
        transform.DOKill();
        transform.localScale = _initialSize;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_isCaged)
        {
            return;
        }

        _gridManager.CheckForLink(this);
    }

    public void ResetLine()
    {
        UILineRenderer.points[1] = Vector2.zero;
        UILineRenderer.SetAllDirty();
        StopTracking();
        StopAnimation();
    }

    public void WasMatched()
    {
        _wasMatched = true;
    }

    public bool CheckIfBallWasMatched() => _wasMatched;

    public RectTransform GetRectParentTransform()
    {
        return _parentRectTransform;
    }
}
