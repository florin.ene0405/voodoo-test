using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;

    public AudioSource AudioSource;
    public AudioClip ButtonClick;
    public AudioClip MatchSound;
    public AudioClip WinSound;
    public AudioClip LoseSound;
    public AudioClip BackgroindSound;

    public List<AudioClip> MatchSounds = new List<AudioClip>();
    #region Singleton
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    #endregion

    private void Start()
    {
        AudioSource.clip = BackgroindSound;
        AudioSource.Play();
    }

    public void PlaySound(string sound)
    {
        switch (sound)
        {
            case Constantes.Sounds.BUTTON_CLICK:
                AudioSource.PlayClipAtPoint(ButtonClick, Vector3.zero);
                break;
            case Constantes.Sounds.MATCH_SOUND:
                AudioSource.PlayClipAtPoint(MatchSound, Vector3.zero);
                break;
            case Constantes.Sounds.WIN_SOUND:
                AudioSource.PlayClipAtPoint(WinSound, Vector3.zero);
                AudioSource.Stop();
                break;
            case Constantes.Sounds.LOSE_SOUND:
                AudioSource.PlayClipAtPoint(LoseSound, Vector3.zero);
                AudioSource.Stop();
                break;
            default:
                break;
        }
    }

    public void PlayMatchSound(int index)
    {
        if (index >= MatchSounds.Count || index  < 0)
        {
            return;
        }

        AudioSource.PlayClipAtPoint(MatchSounds[index], Vector3.zero);
    }
}