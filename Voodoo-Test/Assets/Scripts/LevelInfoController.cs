using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LevelInfoController : MonoBehaviour
{
    public GameManager GameManager;
    public TextMeshProUGUI MoveCounter;
    public ColorBallsInfo ColorBallsInfoPrefab;

    private List<ColorBallsInfo> _allInfo = new List<ColorBallsInfo>();
    private int _moves;

    public void Initialize(LevelSO level)
    {
        _moves = level.Moves;
        MoveCounter.text = _moves.ToString();

        for (int i = 0; i < level.ColorBalls.Count; i++)
        {
            if (level.ColorBalls[i].Number <= 0 )
            {
                continue;
            }

            ColorBallsInfo newInfo = Instantiate<ColorBallsInfo>(ColorBallsInfoPrefab, transform);
            newInfo.Initialize(level.ColorBalls[i].Color, level.ColorBalls[i].Number, level.ColorBalls[i].ColorName);
            _allInfo.Add(newInfo);
        }
    }

    public void NewMatch(BallController element)
    {
        if (element.IsCaged)
        {
            return;
        }

        for (int i = 0; i < _allInfo.Count; i++)
        {
            if (string.Equals(element.ColorName, _allInfo[i].ColorName))
            {
                _allInfo[i].LowerTheMatchedNeeded();
            }
        }
    }

    public void NewMove(bool powerUps)
    {
        if (!powerUps)
        {
            _moves--;
        }

        MoveCounter.text = _moves.ToString();

        if (_moves <= 0)
        {
            OnGameLose();
            return;
        }


        CheckForWin();
    }

    private void CheckForWin()
    {
        for (int i = 0; i < _allInfo.Count; i++)
        {
            if (_allInfo[i].Counter > 0)
            {
                return;
            }
        }

        OnGameWin();
    }

    private void OnGameWin()
    {
        GameManager.OnGameWin();
    }

    private void OnGameLose()
    {
        GameManager.OnGameLose();
    }
}
